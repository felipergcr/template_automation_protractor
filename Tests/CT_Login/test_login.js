/**
 * Created by roque on 31/01/18.
 */
var Jasmine2HtmlReporter = require('protractor-jasmine2-html-reporter');

describe('Funcionalidade a ser Testada', function(){
    let page_home = require('../../Pages/Home/page_home.js');
    let utils = require('../../utils.js');
    var originalTimeout;

    beforeEach(function (done) {
        originalTimeout = jasmine.DEFAULT_TIMEOUT_INTERVAL;
        jasmine.DEFAULT_TIMEOUT_INTERVAL = 1000000;
        setTimeout(function () {
            browser.ignoreSynchronization = true;
            browser.waitForAngular();
            browser.get(utils.path() + ':8000/#/login');
            done();
        }, 100);
    });

    afterEach(function(){
        jasmine.DEFAULT_TIMEOUT_INTERVAL = originalTimeout;
        browser.restart();

    });

    afterAll(function(done){
        process.nextTick(done);
        browser.close();
    });

    it("CT1- CT1 traqueado com o Testlink", function() {
        utils.loginApplication('123789','123');
        page_home.waitScreenHome();
        page_home.clickMaterialInputMenu();
        expect(browser.getCurrentUrl()).toBe(utils.path() + ':8000/#/material-input');
    });

    it("CT2- CT2 traqueado com o TestLink", function() {
        utils.loginApplication('123789','123');
        page_home.waitScreenHome();
        page_home.clickMaterialInputMenu();
        browser.sleep(2000);
        let textBtn = element(by.id("btn-add-loans")).getText();
        expect(textBtn).toEqual("+ ADICIONAR");
    })
});