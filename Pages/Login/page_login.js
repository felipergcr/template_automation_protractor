/**
 * @author roque;
 */
let page_login = function(){

    this.waitScreenLogin = function(){
        var EC = protractor.ExpectedConditions;
        let result = browser.driver.wait(() => {
            return true;
        });
        result.then((value) => {
            let email = element(by.id('email'));
            browser.wait(EC.visibilityOf(email), 5000);
        }).catch('Erro');
    };
    this.clickButtonEnter = function(){
        element(by.id('btn-entrar')).click();
    };
    this.enterCode = function(code){
        element(by.id('code')).sendKeys(code);
    };
    this.enterPassword = function(password){
        element(by.id('password')).sendKeys(password);
    };
};
module.exports = new page_login();