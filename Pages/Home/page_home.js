/**
 * Created by roque on 02/06/17.
 */
let page_home = function(){

    this.waitScreenHome = function(){

        var EC = protractor.ExpectedConditions;
        let result = browser.driver.wait(() => {
            return true;
        });
        result.then((value) => {
            let home = element(by.id('home'));
            browser.wait(EC.visibilityOf(home), 5000);
        }).catch('Erro');
    };

    /**
     * Menu
     */
    this.clickMaterialInputMenu = function(){
        element(by.id('material-input')).click();
    };
};
module.exports = new page_home();