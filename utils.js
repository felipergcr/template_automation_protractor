/**
 * Created by roque on 31/01/18.
 */
module.exports = {
    /*
     * @author roque
     *
     */
    path: function () {
        //IP local
        return "http://localhost";
    },
    /**
     * @author roque
     */
    loginApplication: function (code,password){

        let page_login = require('./Pages/Login/page_login');

        page_login.enterCode(code);
        page_login.enterPassword(password);
        page_login.clickButtonEnter();

        expect(browser.getCurrentUrl()).toBe(this.path() + ':8000/#/home');
    }
};
