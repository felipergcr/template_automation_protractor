var Jasmine2HtmlReporter = require('protractor-jasmine2-html-reporter');

var today = new Date(),
    timeStamp = today.getMonth() + 1 + '-' + today.getDate() + '-' + today.getFullYear() + '-' + today.getHours() + 'h-' + today.getMinutes() + 'm';

var reporter = new Jasmine2HtmlReporter({

     savePath: 'ReportTestResult/'
    ,showSummary: true
    ,showQuickLinks: true
    ,showConfiguration: true
    ,screenshotsFolder: 'Screenshots'
    ,takeScreenshots: true
    ,takeScreenshotsOnlyOnFailures: true
    ,fixedScreenshotName: true
    ,showPassed: true
    ,consolidate: true
    ,consolidateAll: false
    ,fileName: 'Report'
    ,fileNameSeparator: '_'
    ,fileNameDateSuffix: true
    ,fileNameSeparator: '_'
    ,cleanDestination: false
});

exports.config = {
    seleniumAddress: 'http://localhost:4444/wd/hub',
    directConnect: false,

    highlightDelay: 200,

    multiCapabilities: [
        {'browserName': 'firefox'}
        // {'browserName': 'chrome'}

    ],
    baseUrl:'http:localhost:8080/#/',

    specs: [
        /**
         *  Template
         */
        'Tests/CT_Login/test_login.js',

    ],

    framework: 'jasmine2',

    // ----- Options to be passed to minijasminenode -----
    jasmineNodeOpts: {
        // onComplete will be called just before the driver quits.
        onComplete: null,
        // If true, display spec names.
        isVerbose: false,
        // If true, print colors to the terminal.
        showColors: true,
        // If true, include stack traces in failures.
        includeStackTrace: true,
        // Default time to wait in ms before a test fails.
        // defaultTimeoutInterval: 10000
    },
    onPrepare: function() {
        browser.driver.manage().window().maximize();
        jasmine.getEnv().addReporter(reporter);
        var Reporter = require('jasmine-spec-reporter').SpecReporter;
        jasmine.getEnv().addReporter(new Reporter({

             displayFailuresSummary: true // display summary of all failures after execution
            ,displayFailedSpec: true      // display each failed spec
            ,displaySuiteNumber: true    // display each suite number (hierarchical)
            ,displaySpecDuration: true   // display each spec duration
            ,colors: {
                success: 'green'
                ,failure: 'red'
                ,pending: 'yellow'
            }
            ,prefixes: {
                success: '✓ '
                ,failure: '✗ '
                ,pending: '* '
            }
        }));
    }


};